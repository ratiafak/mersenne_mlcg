/*
 * lcg32.h
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#ifndef LCG32_H_
#define LCG32_H_

#include <stdint.h>

typedef uint32_t lcg32_state_t;	///< Internal state of the generator

/**
 * @brief Initialize internal state of this generator.
 * @param seed 32 bit number
 * @return internal state to be used from now on
 */
lcg32_state_t lcg32_srand(uint32_t seed);

/**
 * @brief Generate pseudorandom number.
 * @param state internal state
 * @return one 32 bit number
 */
uint32_t lcg32_rand(lcg32_state_t* state);

#endif /* LCG32_H_ */
