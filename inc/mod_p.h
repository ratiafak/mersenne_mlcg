/*
 * mod_p.h
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#ifndef MOD_P_H_
#define MOD_P_H_

#include <stdint.h>

#define MOD_P_P_MERS   (31)
#define MOD_P_P        ((1ULL << MOD_P_P_MERS) - 1)

/**
 * @brief Calculate modulo mersenne prime.
 * @param x input number
 * @return x%MOD_P_P, where MOD_P_P is mersenne prime 2^MOD_P_P_MERS - 1
 */
uint32_t mod_p_calculate(uint64_t x);

#endif /* MOD_P_H_ */
