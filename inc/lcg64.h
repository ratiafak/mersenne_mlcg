/*
 * lcg64.h
 *
 *  Created on: Nov 23, 2018
 *      Author: Tomas Jakubik
 */

#ifndef LCG64_H_
#define LCG64_H_

#include <stdint.h>

typedef uint64_t lcg64_state_t;	///< Internal state of the generator

/**
 * @brief Initialize internal state of this generator.
 * @param seed 64 bit number
 * @return internal state to be used from now on
 */
lcg64_state_t lcg64_srand(uint64_t seed);

/**
 * @brief Generate pseudorandom number.
 * @param state internal state
 * @return one 64 bit number
 */
uint64_t lcg64_rand(lcg64_state_t* state);

#endif /* LCG64_H_ */
