/**
 * @file main.c
 * @brief Main file of a math exam project to test mersenne prime mlcg.
 * @date 22 Nov 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include "mers_mlcg.h"
#include "lcg32.h"
#include "lcg64.h"

#define SEED   ((time(NULL) << 32) | (time(NULL) ^ 0x4d531dc1))
//#define SEED   87465126

//#define TYPE 1 Type defined in build configuration

/**
 * @brief Main function of math exam project.
 * @return nonzero on error
 */
int main(void)
{
#if TYPE == 0
	mers_mlcg_state_t mlcg_state = mers_mlcg_srand(SEED);	//Init generator
#elif TYPE == 1
	lcg32_state_t lcg32_state = lcg32_srand(SEED);	//Init generator
#elif TYPE == 2
	mers_mlcg_state_t mlcg_state = mers_mlcg_srand(SEED);	//Init generator
	lcg32_state_t lcg32_state = lcg32_srand(SEED);	//Init generator
#elif TYPE == 3
	lcg64_state_t lcg64_state = lcg64_srand(SEED);	//Init generator
	uint64_t remainder = 0;
#endif
	uint32_t out;

	while(1)
	{
#if TYPE == 0
		out = mers_mlcg_rand(&mlcg_state);	//Get random 32 bits
#elif TYPE == 1
		out = lcg32_rand(&lcg32_state);	//Get random 32 bits
#elif TYPE == 2
		out = lcg32_rand(&lcg32_state) ^ mers_mlcg_31b_rand(&mlcg_state);	//Combine the generators
#elif TYPE == 3
		if(remainder)
		{
			out = remainder;
			remainder = 0;
		}
		else
		{
			remainder = lcg64_rand(&lcg64_state);	//Get random 64 bits
			out = remainder;
			remainder >>= 32;
		}
#endif
		fwrite(&out, 1, 4, stdout);	//Put 32 bits to stdout
	}

	return 0;
}



