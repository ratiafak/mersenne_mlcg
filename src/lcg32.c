/*
 * lcg32.c
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#include "lcg32.h"

#define LCG32_A     2154523453
#define LCG32_C     875429873

/**
 * @brief Initialize internal state of this generator.
 * @param seed 32 bit number
 * @return internal state to be used from now on
 */
lcg32_state_t lcg32_srand(uint32_t seed)
{
	return seed;
}

/**
 * @brief Generate pseudorandom number.
 * @param state internal state
 * @return one 32 bit number
 */
uint32_t lcg32_rand(lcg32_state_t* state)
{
	*state = *state*LCG32_A + LCG32_C;	//Generate new number
	return *state;
}

