/*
 * mod_p.c
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#include "mod_p.h"

/**
 * @brief Calculate modulo mersenne prime.
 * @param x input number
 * @return x%MOD_P_P, where MOD_P_P is mersenne prime 2^MOD_P_P_MERS - 1
 */
uint32_t mod_p_calculate(uint64_t x)
{
	uint64_t out = (x >> MOD_P_P_MERS) + (x & MOD_P_P);	//x / w + x % w
	while(out >= MOD_P_P) out -= MOD_P_P;	//May be replaced by if when input is limited enough
	return out;
}
