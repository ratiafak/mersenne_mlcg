/*
 * lcg64.c
 *
 *  Created on: Nov 23, 2018
 *      Author: Tomas Jakubik
 */

#include "lcg64.h"

#define LCG64_A     6364136223846793005	//Suggested by Knuth
#define LCG64_C     1442695040888963407

/**
 * @brief Initialize internal state of this generator.
 * @param seed 64 bit number
 * @return internal state to be used from now on
 */
lcg64_state_t lcg64_srand(uint64_t seed)
{
	return seed;
}

/**
 * @brief Generate pseudorandom number.
 * @param state internal state
 * @return one 64 bit number
 */
uint64_t lcg64_rand(lcg64_state_t* state)
{
	*state = *state*LCG64_A + LCG64_C;	//Generate new number
	return *state;
}

